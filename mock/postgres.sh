#!/usr/bin/env sh

docker run \
    -e POSTGRES_PASSWORD=test \
    -it \
    -v ./postgres:/var/lib/postgresql/data \
    -p 0.0.0.0:5432:5432 \
    postgres:14
