#!/usr/bin/env python

import uuid
import hashlib
import psycopg2

conn = psycopg2.connect("user=postgres password=test host=0.0.0.0 dbname=hashes")

RANGE = 4294967295

cur = conn.cursor()
for entry in range(RANGE):
    hash = hashlib.sha256(str(uuid.uuid4()).encode()).digest()
    cur.execute("""INSERT INTO hashes(id) VALUES(%s);""", (hash, ))
    if entry % 100000 == 0:  # Print progress every 100,000 commits
        conn.commit()
        print(entry)
        print(hash)
conn.close()
