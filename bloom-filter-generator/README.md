# bloom-filter-generator

This code moved to https://git.distrust.co/milksad/rust-bloom-filter-generator.

## Usage note

Make sure to use a suitable parameter value for bloom filter size that corresponds to the input data set.

Experiment with the false positive factor parameter for memory footprint vs. accuracy trade-off.