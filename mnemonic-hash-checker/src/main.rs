use std::net::SocketAddr;
use std::path::PathBuf;
use std::sync::Arc;

use axum::{
    extract::{Query, State},
    http::{header::CONTENT_TYPE, StatusCode},
    response::{AppendHeaders, IntoResponse},
    routing::get,
    Router,
};

use bloomfilter::Bloom;

use color_eyre::eyre::Result;

use clap::Parser;

use deadpool_postgres::{Config, ManagerConfig, Pool, RecyclingMethod, Runtime};
use tokio_postgres::NoTls;

use serde::Deserialize;

use tower_http::catch_panic::CatchPanicLayer;
use tower_http::trace::TraceLayer;

use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use tracing_subscriber::fmt::format::FmtSpan;
use tracing_subscriber::prelude::*;

use tracing::{error, info};

mod bloom;

#[derive(Parser)]
struct CliConfig {
    #[clap(long, env)]
    bloom_filter_path: PathBuf,

    #[clap(long, env, default_value = "lookup")]
    postgres_dbname: String,

    #[clap(long, env, default_value = "localhost")]
    postgres_host: String,

    #[clap(long, env, default_value_t = 5432)]
    postgres_port: u16,

    #[clap(long, env = "USER", default_value = "postgres")]
    postgres_user: String,

    #[clap(long, env)]
    postgres_password: Option<String>,

    #[clap(long, env, default_value = "hashes")]
    database_table: String,

    #[clap(long, env, default_value = "hash")]
    database_column: String,

    #[clap(long, env)]
    bind_address: Option<SocketAddr>,
}

struct AppState {
    pool: Pool,
    database_query: String,
    bloom_filter: Bloom<String>,
}

fn setup_registry() {
    let envfilter = EnvFilter::builder()
        .with_default_directive(LevelFilter::DEBUG.into())
        .from_env_lossy();
    tracing_subscriber::registry()
        .with(envfilter)
        .with(tracing_subscriber::fmt::layer().with_span_events(FmtSpan::CLOSE))
        .with(tracing_error::ErrorLayer::default())
        .init();
}

async fn index() -> impl IntoResponse {
    (
        AppendHeaders([(CONTENT_TYPE, "text/html")]),
        include_str!("index.html"),
    )
}

#[tracing::instrument(skip(state))]
async fn check_hash_in_db(hash: &str, state: &AppState) -> Result<bool> {
    let client = state.pool.get().await?;
    let query = client.prepare_cached(&state.database_query).await?;
    let rows = client.query(&query, &[&hash]).await?;

    if let Some(row) = rows.get(0) {
        let retrieved_hash: String = row.try_get(0)?;
        Ok(hash == retrieved_hash)
    } else {
        Ok(false)
    }
}

#[derive(Deserialize)]
struct CheckQuery {
    sha256: String,
}

const VULNERABLE: &'static str = "vulnerable";
const NOT_FOUND: &'static str = "match not found";

// Note: Exposes *zero* information of potential errors to clients.
#[tracing::instrument(skip(query, state))]
async fn check_hash_slug(
    query: Query<CheckQuery>,
    State(state): State<Arc<AppState>>,
) -> (StatusCode, &'static str) {
    let hash = query.sha256.to_string();
    match state.bloom_filter.check(&hash) {
        true => {
            info!("Found hash in bloom filter, looking in db");
            match check_hash_in_db(&hash, &state).await {
                Ok(true) => (StatusCode::OK, VULNERABLE),
                Ok(false) => (StatusCode::NOT_FOUND, NOT_FOUND),
                Err(e) => {
                    error!(%e, "Error while handling postgres lookup");
                    (StatusCode::NOT_FOUND, NOT_FOUND)
                }
            }
        }
        false => (StatusCode::NOT_FOUND, NOT_FOUND),
    }
}

const MILK_SAD: &str = concat!(
    "1f76da7a36fcacfe22b6c68d1acf6204",
    "73744e78aa04242a40374953783706f0",
);

#[tokio::main]
async fn main() -> Result<()> {
    setup_registry();
    color_eyre::install()?;

    let cli_config = CliConfig::parse();
    let addr = cli_config
        .bind_address
        .unwrap_or_else(|| (std::net::Ipv4Addr::new(127, 0, 0, 1), 8000).into());

    info!("configuring database pool");

    let mut db_config = Config::new();
    db_config.dbname = Some(cli_config.postgres_dbname);
    db_config.host = Some(cli_config.postgres_host);
    db_config.password = cli_config.postgres_password;
    db_config.port = Some(cli_config.postgres_port);
    db_config.user = Some(cli_config.postgres_user);
    db_config.manager = Some(ManagerConfig {
        recycling_method: RecyclingMethod::Fast,
    });
    let pool = db_config.create_pool(Some(Runtime::Tokio1), NoTls)?;

    info!("testing db connection");

    let database_query = format!(
        "SELECT encode({column}, 'hex') FROM {table} WHERE hash = decode($1, 'hex')",
        column = cli_config.database_column,
        table = cli_config.database_table
    );

    {
        let client = pool.get().await?;
        let query = client.prepare_cached(&database_query).await?;
        let rows = client.query(&query, &[&MILK_SAD]).await?;
        let row = rows.get(0).expect("db row");
        let column: String = row.try_get(0).expect("db column");
        assert_eq!(column, MILK_SAD);
    }

    info!("loading bloom filter");

    let state = AppState {
        bloom_filter: bloom::load(cli_config.bloom_filter_path.as_ref())?,
        pool,
        database_query,
    };

    let app = Router::new()
        .route("/", get(index))
        .route("/check", get(check_hash_slug))
        .with_state(Arc::new(state))
        .layer(CatchPanicLayer::new())
        .layer(TraceLayer::new_for_http());

    info!("server go nyoom: {addr}");

    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await?;

    Ok(())
}
