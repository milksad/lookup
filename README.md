# Milk Sad Lookup

Insert many rows of sha256 hashes into a Postgres database.

1. Install `postgress-client`:

```shell
sudo apt install postgres-client
```

2. Install Docker
3. Install Python dependencies:

```shell
pip install psycopg2
```

4. Run the database via Docker:

```shell
./mock/postgres.sh
```

5. Configure the database:
- Log into `psql shell`:

```shell
psql -h localhost -p 5432 -U postgres
```

Password: `test`

- Create Database, table, and index:

```shell
postgres=# CREATE DATABASE hashes;
postgres=# \c hashes;
hashes=# CREATE TABLE hashes(id BYTEA);
hashes=# CREATE INDEX hashes_id on hashes(id);
```

6. Hammer time:

```shell
./mock/insert_into.py
```